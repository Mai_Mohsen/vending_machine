
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Mai
 */
public class Inventory <T>{
    private Map<T ,Integer> inventory = new HashMap <T ,Integer>();
    
    public int getQuantity(T t){
        int value = inventory.get(t);
        if (value >0)
            return value;
        else {
            System.out.println("Out off this item");
            
            
            
            
            return 0;
        
        }
    
    }
    
    public void addItem(T t){
        int count = inventory.get(t);
        inventory.put(t, count+1);
    
    }
    
    public void subtracktItem(T t){
        
      int count = inventory.get(t);
       inventory.put(t, count-1);
    }
    
    public boolean hasItem(T t){
        return getQuantity(t) >0;
    
    }
    
    public void putNewItem(T t, int i)
    
    { inventory.put(t, i); 
    
    }
    
}
