/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;

/**
 *
 * @author Mai
 */
public class VendingMachine {
    
    
    
    
    
     private static Inventory<Coin> cionInventory = new Inventory<Coin>();
    private static Inventory<Item> itemInventory = new Inventory<Item>();
    private static ArrayList<Coin> custmoerCoins = new ArrayList<Coin>();
    private static ArrayList<Item> custmoerItems = new ArrayList<Item>();
    
    private static int custmoerBalance;
    
    
    
    public VendingMachine(){
        
        for (Coin c :Coin.values())
            cionInventory.putNewItem(c, 3);
        
        for (Item t :Item.values())
            itemInventory.putNewItem(t, 3);
    }
    
    
    public static void insertCoin(Coin c){
        
        custmoerBalance = custmoerBalance + c.getValue();
        cionInventory.addItem(c);
        custmoerCoins.add(c);
    
    }
    
    
    public static int selectAndPrice(Item t){
        
        if (!itemInventory.hasItem(t))
            throw new SoldOutException ("This item have been sold out , Please choose another one");
        else{
            itemInventory.subtracktItem(t);
            custmoerItems.add(t);
            return t.getPrice();
            }
            }
    
    
    public static void removeItem(Item t){
        if (custmoerItems.contains(t)){
            custmoerItems.remove(t);
            itemInventory.addItem(t);}
    
    
    }
    private static int getTotalPrice(){
        
        int totalPrice=0;
        if (custmoerItems.isEmpty())
            return 0;
        for (int i = 0; i < custmoerItems.size(); i++)
            totalPrice = totalPrice + custmoerItems.get(i).getPrice();
        return totalPrice;
    
    }
    
    
  
    public static Bill checkOut(){

        if(custmoerBalance >= getTotalPrice()){
            Bill b = new Bill(custmoerBalance, custmoerItems, getTotalPrice());
            int change = custmoerBalance - getTotalPrice();
            ArrayList<Coin> changes = new ArrayList<Coin>();
            while(change>0){
                if (change >= Coin.TOW_DOLLARS.getValue() && cionInventory.hasItem(Coin.TOW_DOLLARS)){
                    changes.add(Coin.TOW_DOLLARS);
                    change = change - Coin.TOW_DOLLARS.getValue();
                    cionInventory.subtracktItem(Coin.TOW_DOLLARS);
                    
                }
                
                else if (change >= Coin.DOLLAR.getValue() && cionInventory.hasItem(Coin.DOLLAR)){
                    changes.add(Coin.DOLLAR);
                    change = change - Coin.DOLLAR.getValue();
                    cionInventory.subtracktItem(Coin.DOLLAR);
                    
                }
                
                else if (change >= Coin.HALF.getValue() && cionInventory.hasItem(Coin.HALF)){
                    changes.add(Coin.HALF);
                    change = change - Coin.HALF.getValue();
                    cionInventory.subtracktItem(Coin.HALF);
                    
                }
                else if (change >= Coin.QUARTER.getValue() && cionInventory.hasItem(Coin.QUARTER)){
                    changes.add(Coin.QUARTER);
                    change = change - Coin.QUARTER.getValue();
                    cionInventory.subtracktItem(Coin.QUARTER);
                    
                }
                else if (change >= Coin.DIME.getValue() && cionInventory.hasItem(Coin.DIME)){
                    changes.add(Coin.DIME);
                    change = change - Coin.DIME.getValue();
                    cionInventory.subtracktItem(Coin.DIME);
                    
                }
                else if (change >= Coin.NICKEL.getValue() && cionInventory.hasItem(Coin.NICKEL)){
                    changes.add(Coin.NICKEL);
                    change = change - Coin.NICKEL.getValue();
                    cionInventory.subtracktItem(Coin.NICKEL);
                    
                }
                else if (change >= Coin.PENNY.getValue() && cionInventory.hasItem(Coin.PENNY)){
                    changes.add(Coin.PENNY);
                    change = change - Coin.PENNY.getValue();
                    cionInventory.subtracktItem(Coin.PENNY);
                    
                }
                else
                    throw new NoChangeAvailableException("NotSufficientChange");
            
            
            
            }
            return b;}
        else
            throw new NotSufficientPaidException("Your Balance is not Sufficent, Please insert more coins");
        
            
    
    }
    
    public static void clear(){
    
    custmoerCoins.clear();
    custmoerItems.clear();
    custmoerBalance =0;
    
    }
    
    public static ArrayList<Coin> refund(){
         ArrayList<Coin> money = new ArrayList<Coin>();
        if (custmoerCoins.size() >0){
           
            for(int i = 0; i < custmoerCoins.size(); i++){
                money.add(custmoerCoins.get(i));
                cionInventory.subtracktItem(custmoerCoins.get(i));
      
            }
        }
         custmoerCoins.clear();
         custmoerItems.clear();
         custmoerBalance =0;
        return money;
 
    }
    
    
    public static StringBuilder showInventory(){
    
    StringBuilder items =new StringBuilder("Inventory items \n");
    for (Item t :Item.values()){
        items.append(t.getName() + "      " +itemInventory.getQuantity(t) + "\n");
 }
    
    return items;
    }
    
    
    
    
   

    
}
